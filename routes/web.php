<?php

use App\Http\Controllers\Controller;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/test',function (){
    return ['name'=> 'tavnir'];
});
Route::get('/hello/{name}',function ($name){
    $name = ucfirst($name);
    return "Hello $name";
});

Route::get('/test2',function () {
    $number = 12;
    return view("test",compact('number'));
});

Route::get('test3',function (){
//    $myName = "Taohidil Islam Tanvir";
    $study = "<p>I'm Diploma in civil</p>";
    $laravelIcon = '<i class="fab fa-laravel"></i>';
    return view('user.index',compact('laravelIcon'),compact('study'));

});

Route::get('/user',[UserController::class, 'index']);
